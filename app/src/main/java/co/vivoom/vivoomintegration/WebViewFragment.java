package co.vivoom.vivoomintegration;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.HashMap;
import java.util.Map;

/**
 * A fragment containing the webview.
 */
public class WebViewFragment extends Fragment {

    public static final String USER_AGENT = "Vivoom; Web/Android;";

    private static final String BASE_CAMPAIGN_URL = "https://html5video.vivoom.co";

    private static final String FACEBOOK_DOMAIN = "facebook.com";
    private static final String TWITTER_DOMAIN = "twitter.com";

    private static final int EXTRA_DURATION_LIMIT = 15;
    private static final int REQUEST_PICK_VIDEO = 0;
    private static final int REQUEST_PICK_VIDEO_API_21 = 1;
    private static final String LOG_TAG = "Vivoom; Web/Android";

    private ValueCallback<Uri> valueCallbackCompatibility;
    private ValueCallback<Uri[]> valueCallback;
    private WebView webView;

    public WebViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        webView = new WebView(inflater.getContext());
        webView.getSettings().setUserAgentString(USER_AGENT); // Recommended to have a custom User-Agent, or at least a marker in it.
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webView.getSettings().setDatabasePath("/data/data/" + webView.getContext().getPackageName() + "/databases/");
        }
        CookieManager.getInstance().setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!shouldHandleUrl(url)) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                }
                Log.d(LOG_TAG, "Loading: " + url);
                return false;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Log.e(LOG_TAG, error.toString());
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                final String modifiedUrl = url.endsWith("/") ? url.replaceAll("/$", "") : url;
                if (BASE_CAMPAIGN_URL.equals(modifiedUrl)) {
                    // If the native app that hosts this webview already sets a custom User-Agent
                    // signature, then this block is not needed
                    Log.d(LOG_TAG, "Initializing android webview");
                    webView.loadUrl("javascript: window.vivoom.androidWebView(true);");
                }
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            // file upload callback (Android 4.1 (API level 16) -- Android 4.3 (API level 18))
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> valueCallback, String acceptType, String capture) {
                WebViewFragment.this.valueCallbackCompatibility = valueCallback;
                // TODO must parse acceptType and capture parameters and pass into requestVideoFile
                requestVideoFile();
            }


            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                WebViewFragment.this.valueCallback = filePathCallback;
                requestVideoFile();
//                // TODO It's possible to use FileChooserParams.createIntent() and FileChooserParams.parseResult() methods for API Level 21+ but we haven't tested this approach in depth
//                Intent intent = fileChooserParams.createIntent();
//                startActivityForResult(intent, REQUEST_PICK_VIDEO_API_21);
                return true;
            }
        });
        if (savedInstanceState == null) {
            CookieSyncManager.createInstance(inflater.getContext());
            final Map<String, String> additionalHeaders = new HashMap<>();
            /* add headers here as needed */
            webView.loadUrl(BASE_CAMPAIGN_URL, additionalHeaders);
        } else {
            webView.restoreState(savedInstanceState);
        }

        return webView;
    }

    private boolean shouldHandleUrl(String url) {
        if (url.contains(FACEBOOK_DOMAIN) || url.contains(TWITTER_DOMAIN)) {
            return false;
        }
        return url.startsWith("http") || url.startsWith("https");
    }

    private void requestVideoFile() {
        final Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("video/*");
        galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        final Intent recordVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        recordVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, EXTRA_DURATION_LIMIT);

        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select video");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {recordVideoIntent});
        startActivityForResult(chooserIntent, REQUEST_PICK_VIDEO);
    }

    @Override
    public void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().startSync();
    }

    @Override
    public void onPause() {
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_PICK_VIDEO) {
            if (valueCallback != null) {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(LOG_TAG, intent.getData().toString());
                    valueCallback.onReceiveValue(new Uri[]{intent.getData()});
                } else {
                    valueCallback.onReceiveValue(null);
                }
            } else if (valueCallbackCompatibility != null) {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(LOG_TAG, intent.getData().toString());
                    valueCallbackCompatibility.onReceiveValue(intent.getData());
                } else {
                    valueCallbackCompatibility.onReceiveValue(null);
                }
            }
        } else if (requestCode == REQUEST_PICK_VIDEO_API_21) {
            if (valueCallback != null) {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(LOG_TAG, intent.getData().toString());
                    Uri[] result = parseResultApi21(intent);
                    valueCallback.onReceiveValue(result);
                } else {
                    valueCallback.onReceiveValue(null);
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Uri[] parseResultApi21(Intent intent) {
        return WebChromeClient.FileChooserParams.parseResult(REQUEST_PICK_VIDEO_API_21, intent);
    }

}
