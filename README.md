#Vivoom Webapp Integration

This project demonstrates how to integrate a Vivoom campaign in a native Android application by using a webview.
This example uses a fragment to provide more flexibility regarding the instantiation and layout of the component.

##Compatibility
This project is compatible with Android Lollipop v5.0 and more recent versions

##Permissions
The host application needs to be able to request the following permissions:

`android.permission.INTERNET`: to load data from the web

`android.permission.READ_EXTERNAL_STORAGE`: to load videos from external storage

`android.permission.CAMERA`: Only needed if recording videos

It's also useful to declare a **uses-feature** clause:

`<uses-feature android:name="android.hardware.camera" android:required="false" />`


##Integration checklist

###Open webview
Webview can be instantiated in any of the supported ways, from xml or in code, but it must always be configured to allow javascript execution and persist data.

###Cookie saving/persistence
In the fragments's onCreate() method, add:

```
if (savedInstanceState == null) {
 CookieSyncManager.createInstance(inflater.getContext()); //can be any valid context
 ...
}
```

In `onResume()`:
```
CookieSyncManager.getInstance().startSync();
```

In `onPause()`:
```
CookieSyncManager.getInstance().stopSync();
```

###Handling urls
In order to share to some destinations, it's more convenient sometimes to allow third-party apps to handle it. For instance, we delegate sharing to Facebook, Twitter, e-mail and SMS native clients. You could add or remove destinations by editing or overriding:

```
public boolean shouldOverrideUrlLoading(WebView view, String url)
```

in the webview's WebViewClient.


